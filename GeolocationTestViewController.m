/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import "GeolocationTestViewController.h"
#import <Appcelerator/Appcelerator.h>


@implementation GeolocationTestViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    _startLocation = nil;
}

- (IBAction)resetDistance:(id)sender {
    _startLocation = nil;
}

- (IBAction)returnToPreviousPage:(id)sender {
    [_locationManager stopUpdatingLocation];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    
    [[APSAnalytics sharedInstance] sendAppGeoEvent:newLocation];
    
    NSString *currentLatitude = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.coordinate.latitude];
    _latitude.text = currentLatitude;
    
    NSString *currentLongitude = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.coordinate.longitude];
    _longitude.text = currentLongitude;
    
    NSString *currentHorizontalAccuracy = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.horizontalAccuracy];
    _horizontalAccuracy.text = currentHorizontalAccuracy;
    
    NSString *currentAltitude = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.altitude];
    _altitude.text = currentAltitude;
    
    NSString *currentVerticalAccuracy = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.verticalAccuracy];
    _verticalAccuracy.text = currentVerticalAccuracy;
    
    if (_startLocation == nil)
        _startLocation = newLocation;
    
    CLLocationDistance distanceBetween = [newLocation distanceFromLocation:_startLocation];
    
    NSString *tripString = [[NSString alloc] initWithFormat:@"%f", distanceBetween];
    _distance.text = tripString;
}



@end
