/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import "APSAnalyticsPerformanceAppDelegate.h"
#import <Appcelerator/Appcelerator.h>

@implementation APSAnalyticsPerformanceAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[APSServiceManager sharedInstance] enableWithAppKey:@"52h143R9JXGJ9xF6vDSs8l6NIA6BnEmYSArtbOOmhtOMNmgn/zdRWxaQ5rC7ceesWopVjXYmOiC8YzX/LWOU/N9FtvBP0GhAE9vSADkSzR+KH+h6N+3ubPpgdcMBiGfz8MGdSl+xE9YTNeZex85Yub7omtGYgwZVuxnvBtjGWqgM9Qjds+XL74XFHifTHiP3uHCWWt1nBAC3tWl1yBC6DGwi+Dl7ImwQ3q/Y3SuOrhlRFuZjsVD1/QG0Zpiqhl69DXOGn3KE8OBBGw6B98dYNrBmp1ZDbiVE3CQ7xoxki4jUVezU8EKMwIKb8mc5qUkUvSQA/26jkDPXGspWB9u6fw=="];

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)throwWarningWithTitle:(NSString*)title andMessage:(NSString*)message {
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:title];
    [alert setMessage:message];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}
@end
