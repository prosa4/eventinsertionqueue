/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import "PerformanceTestViewController.h"
#import <Appcelerator/Appcelerator.h>

@interface PerformanceTestViewController ()

@end

@implementation PerformanceTestViewController
@synthesize usernameTextfield;
@synthesize asyncBreadCrumbSwitch;
@synthesize optOutStatusSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.optOutStatusSwitch.on = [APSPerformance sharedInstance].isOptOut;
    self.asyncBreadCrumbSwitch.on = NO;
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view.
    self.usernameTextfield.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)leaveBreadCrumbOnTheFloor:(id)sender {
    static int count =0;
    [[APSPerformance sharedInstance] leaveBreadcrumb:[NSString stringWithFormat:@"Ate some bread . . . . (%d)",++count]];
}

- (IBAction)sendSomeData:(id)sender {
    [APSPerformance sharedInstance][@"RandomKey"] = @"TestValue";
}

- (IBAction)crashTheApp:(id)sender {
    [NSException raise:@"test version 1.0.0 performance" format:@"party crasher"];

}

- (IBAction)throwHandledExceptions:(id)sender {
    
    @try {
        [NSException raise:@"test handled exception" format:@"excellent"];
    }
    @catch (NSException *exception) {
        // Log a handled exception to Crittercism
        [[APSPerformance sharedInstance] logHandledException:exception];
    }
    @finally {
        NSLog(@"Handled Exception Thrown and Caught!  First exception is sent immediately, then a maximum of 3 are sent per minute");
    }
}

- (IBAction)setUsername:(id)sender {
    NSString *value = self.usernameTextfield.text;
    if ((value.length == 0) ||([value isEqualToString:@""])) {
        [self sendAlertWithTitle:@"Empty UserName" andMessage:@"Please enter a valid username"];
    }
    
    [[APSPerformance sharedInstance] setUsername:self.usernameTextfield.text];
}
- (IBAction)asyncModeChanged:(id)sender {
    [[APSPerformance sharedInstance] setAsyncBreadcrumbMode:self.asyncBreadCrumbSwitch.isOn];
}
- (IBAction)optOutSwitchChanged:(id)sender {
    [APSPerformance sharedInstance].optOut = self.optOutStatusSwitch.isOn;
}

- (IBAction)fetchValueForDidCrashOnLastAppLoad:(id)sender {
    [self sendAlertWithTitle:@"DidCrashOnLastAppLoad ?" andMessage:([[APSPerformance sharedInstance] didCrashOnLastAppLoad] ? @"YES" : @"NO" )];
}

- (IBAction)fetchValueForGetOptOutStatus:(id)sender {
    [self sendAlertWithTitle:@"GetOptOutStatus ?" andMessage:([[APSPerformance sharedInstance] isOptOut] ? @"YES" : @"NO" )];
}

- (IBAction)fetchValueForGetUUID:(id)sender {
    [self sendAlertWithTitle:@"UUID" andMessage:[APSPerformance sharedInstance].uniqueIdentifier];
}

- (IBAction)returnMetaData:(id)sender {
    [self sendAlertWithTitle:@"metaData" andMessage:[APSPerformance sharedInstance][@"RandomKey"]];
}

-(void)sendAlertWithTitle:(NSString*)title andMessage:(NSString*)message {
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:title];
    [alert setMessage:message];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

# pragma UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
