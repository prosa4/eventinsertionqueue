/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import <UIKit/UIKit.h>

@interface AnaltyicViewController : UIViewController <UITextFieldDelegate>
- (IBAction)sendNavEvent:(id)sender;
- (IBAction)sendFeatureEvent:(id)sender;
- (IBAction)getDeployType:(id)sender;
- (IBAction)sendSeveralFeatureEvents:(id)sender;
@end
