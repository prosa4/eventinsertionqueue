/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import <UIKit/UIKit.h>

@interface AnaltyicModalViewController : UIViewController
- (IBAction)dissmissme:(id)sender;

@end
