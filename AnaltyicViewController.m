/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import "AnaltyicViewController.h"
#import <Appcelerator/Appcelerator.h>
@interface AnaltyicViewController ()

@end

@implementation AnaltyicViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sendNavEvent:(id)sender {
    [[APSAnalytics sharedInstance] sendAppNavEventFromView:@"main"
                                                    toView:@"someOtherView"
                                                  withName:@"Important View"
                                                   payload:@{@"key1":@"value1",
                                                             @"key2":@"value2"
                                                             }];
}

- (IBAction)sendFeatureEvent:(id)sender {
    [[APSAnalytics  sharedInstance] sendAppFeatureEvent:@"Feature Event"
                                                payload:@{@"key1":@"value1",
                                                          @"key2":@"value2"}];

}

- (IBAction)sendSeveralFeatureEvents:(id)sender {
    for (int i = 0; i < 100; i++) {
        [[APSAnalytics  sharedInstance] sendAppFeatureEvent:[NSString stringWithFormat:@"Event-%d", i]
                                                    payload:@{@"key1":@"value1",
                                                              @"key2":@"value2"}];
        // [NSThread sleepForTimeInterval:.1];
        NSLog(@"Sending Event-%d", i);
    }
}

- (IBAction)getDeployType:(id)sender {
    [self sendAlertWithTitle:@"Current DeployType :"
                  andMessage:[[APSAnalytics sharedInstance] deployType]];
}


-(void)sendAlertWithTitle:(NSString*)title andMessage:(NSString*)message {
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:title];
    [alert setMessage:message];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

# pragma UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
