/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

#import <UIKit/UIKit.h>

@interface PerformanceTestViewController : UIViewController <UITextFieldDelegate>

- (IBAction)leaveBreadCrumbOnTheFloor:(id)sender;
- (IBAction)sendSomeData:(id)sender;
- (IBAction)crashTheApp:(id)sender;
- (IBAction)throwHandledExceptions:(id)sender;
- (IBAction)setUsername:(id)sender;
- (IBAction)fetchValueForDidCrashOnLastAppLoad:(id)sender;
- (IBAction)fetchValueForGetOptOutStatus:(id)sender;
- (IBAction)fetchValueForGetUUID:(id)sender;

@property (strong, nonatomic) IBOutlet UISwitch *asyncBreadCrumbSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *optOutStatusSwitch;

@property (strong, nonatomic) IBOutlet UITextField *usernameTextfield;

@end
